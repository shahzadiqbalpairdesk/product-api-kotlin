package com.electro.ktstore.domainframework

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


interface PersistancePort<T> {
    fun save(domo: T): Mono<T>
    fun deleteById(id: String): Mono<Void>
    fun findById(id: String): Mono<T>
    fun findAllById(ids: Iterable<String>): Flux<T>
    fun findAll(): Flux<T>
    fun findOne(example: T): Mono<T>
    fun findAll(example: T): Flux<T>
}