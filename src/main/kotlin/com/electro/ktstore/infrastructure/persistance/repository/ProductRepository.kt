package com.electro.ktstore.infrastructure.persistance.repository

import com.electro.ktstore.infrastructure.persistance.model.ProductDBO
import org.springframework.data.mongodb.repository.ReactiveMongoRepository

interface ProductRepository : ReactiveMongoRepository<ProductDBO,String> {

}