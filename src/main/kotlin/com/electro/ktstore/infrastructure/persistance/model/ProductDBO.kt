package com.electro.ktstore.infrastructure.persistance.model

import org.springframework.data.mongodb.core.mapping.Document

@Document
data class ProductDBO (val id:String,
                       val name:String,
                       val price:Int,
                       val description:String)