package com.electro.ktstore.infrastructure.persistance

import com.electro.ktstore.domain.model.Product
import com.electro.ktstore.domain.ports.ProductPersistancePort
import com.electro.ktstore.infrastructure.persistance.model.ProductDBO
import com.electro.ktstore.infrastructure.persistance.repository.ProductRepository
import org.bson.types.ObjectId
import org.springframework.data.domain.Example
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class ProductMongoPersistancePort(
        private val productRepository: ProductRepository
        ):ProductPersistancePort {

    override fun save(domo: Product) = productRepository.save(toDBO(domo)).map { toDO(it) }

    override fun deleteById(id: String) =  productRepository.deleteById(id)

    override fun findById(id: String) = productRepository
            .findById(id)
            .map { toDO(it) }

    override fun findAllById(ids: Iterable<String>): Flux<Product> {
        TODO("Not yet implemented")
    }

    override fun findAll() = productRepository
            .findAll()
            .map { toDO(it) }

    override fun findAll(example: Product): Flux<Product> {
        TODO("Not yet implemented")
    }

    override fun findOne(example: Product): Mono<Product> {
        val exampleDBO = Example.of(toDBO(example))
        return productRepository.findOne(exampleDBO).map { toDO(it) }
    }

    fun toDBO(product: Product) = ProductDBO(
            if (product.id == "AUTO_GENERATED_ID") ObjectId.get().toHexString() else product.id,
            product.name,
            product.price,
            product.description
    )

    fun toDO(dbo: ProductDBO) = Product(dbo.id, dbo.name, dbo.price,dbo.description)
}