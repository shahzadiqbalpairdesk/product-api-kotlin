package com.electro.ktstore.infrastructure.controller

import com.electro.ktstore.domain.ProductService
import com.electro.ktstore.domain.model.CreateProductCommand
import com.electro.ktstore.domain.model.UpdateProductCommand
import lombok.extern.slf4j.Slf4j
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
@Slf4j
@RequestMapping("/api/product")
class ProductController(private val productService: ProductService) {

    @GetMapping
    fun getAllProducts() = productService.getAllProducts();

    @PostMapping
    fun createProduct(@RequestBody createProductCommand: CreateProductCommand)=productService.createProduct(createProductCommand)

    @GetMapping("/{id}")
    fun getProductById(@PathVariable id:String) = productService.getProductById(id)

    @DeleteMapping("/{id}")
    fun deleteProduct(@PathVariable id:String) = productService.deleteProductById(id)

    @PutMapping("/{id}")
    fun updateProduct(@PathVariable id:String,@RequestBody updateProductCommand: UpdateProductCommand) = productService.updateProduct(id,updateProductCommand)






}