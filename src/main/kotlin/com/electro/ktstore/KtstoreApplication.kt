package com.electro.ktstore

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KtstoreApplication

fun main(args: Array<String>) {
	runApplication<KtstoreApplication>(*args)
}
