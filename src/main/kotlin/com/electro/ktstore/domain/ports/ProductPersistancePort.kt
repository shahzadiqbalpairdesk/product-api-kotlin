package com.electro.ktstore.domain.ports

import com.electro.ktstore.domain.model.Product
import com.electro.ktstore.domainframework.PersistancePort


interface ProductPersistancePort :PersistancePort<Product> {

}
