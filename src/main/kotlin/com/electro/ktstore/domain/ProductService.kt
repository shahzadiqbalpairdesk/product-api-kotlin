package com.electro.ktstore.domain

import com.electro.ktstore.domain.model.CreateProductCommand
import com.electro.ktstore.domain.model.Product
import com.electro.ktstore.domain.model.UpdateProductCommand
import com.electro.ktstore.domain.ports.ProductPersistancePort
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class ProductService(private val productPersistancePort: ProductPersistancePort)  {


     fun getAllProducts() = productPersistancePort.findAll();

     fun createProduct(command: CreateProductCommand): Mono<Product> {
        val createdProduct = Product(
                name = command.name,
                price = command.price,
                description = command.description
        )
        return productPersistancePort.save(createdProduct);
    }

     fun getProductById(id: String) = productPersistancePort.findById(id)

     fun deleteProductById(id: String) = productPersistancePort.deleteById(id)

     fun updateProduct(id: String, command: UpdateProductCommand): Mono<Product> {
                return productPersistancePort.findById(id)
                .flatMap {
                    val updatedProduct: Product = Product(
                            it.id,
                            command.name?:it.name,
                            command.price?:it.price,
                            command.description?:it.description)
                    productPersistancePort.save(updatedProduct)
                }

    }

}