package com.electro.ktstore.domain.model

data class CreateProductCommand(
        val name: String,
        val price: Int,
        val description :String
)

data class UpdateProductCommand(
        val name: String?,
        val price: Int?,
        val description :String?
)