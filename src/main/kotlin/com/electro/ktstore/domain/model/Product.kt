package com.electro.ktstore.domain.model

class Product(
        val id: String = "AUTO_GENERATED_ID",
        val name:String,
        val price:Int,
        val description:String) {
}