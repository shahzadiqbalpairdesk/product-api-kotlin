package com.electro.ktstore.infrastructure.service

import com.electro.ktstore.domain.ProductService
import com.electro.ktstore.domain.model.CreateProductCommand
import com.electro.ktstore.domain.model.Product
import com.electro.ktstore.domain.model.UpdateProductCommand
import com.electro.ktstore.domain.ports.ProductPersistancePort
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.test.StepVerifier


@ExtendWith(MockKExtension::class)
internal class ProductServiceTest {

    private val productPersistancePort  = mockk<ProductPersistancePort>()
    private val productService = ProductService(productPersistancePort)

    @Test
    fun `Should return all product from service`(){
        //arrange
        val product1 = Product("ABC1","hp",200,"hp latops")
        val product2 = Product("ABC2","dell",100,"dell latops")
        every { productPersistancePort.findAll() } returns Flux.just(product1,product2)
        //act
        val result = productService.getAllProducts()
        //assert
        StepVerifier.create(result)
            .expectNext(product1)
            .expectNext(product2)
            .verifyComplete()
    }

    @Test
    fun `Should get product By Id`(){
        //arrange
        val id = "ABC1"
        val product1 = Product(id,"hp",200,"hp latops")
        every { productPersistancePort.findById(id) } returns Mono.just(product1)
        //act
        val result = productService.getProductById(id)
        //assert
        StepVerifier.create(result)
            .expectNextCount(1)
            .verifyComplete()
    }

    @Test
    fun `Should create a product `(){
        //arrange
        val command = CreateProductCommand("Levono",180,"Lenovo laptop")
        val createdProduct = Product(
            name = command.name,
            price = command.price,
            description = command.description
        )
        every { productPersistancePort.save(any()) } returns Mono.just(createdProduct)
        //act
        val result = productService.createProduct(command)
        //assert
        verify { productPersistancePort.save(any()) }
        StepVerifier.create(result)
            .expectNextMatches{it.name == command.name}
            .verifyComplete()
    }

    @Test
    fun `Should edit a product`(){
        //arrange
        val id = "ABC3"
        val command = UpdateProductCommand("Lenovo1",180,"Lenovo laptops")
        val product = Product(id,"Lenovo",180,"Lenovo laptops")
        val updatedProduct =  Product(
            id,
            command.name?:"Lenovo",
            command.price?:180,
            command.description?:"Lenovo laptops"
        )
        every { productPersistancePort.findById(any()) } returns Mono.just(product)
        every { productPersistancePort.save(any()) } returns Mono.just(updatedProduct)
        //act
        val result = productService.updateProduct(id,command)
        //assert
        verify { productPersistancePort.findById(any()) }
        StepVerifier.create(result)
            .expectNextMatches{it.name == command.name}
            .verifyComplete()

    }

    @Test
    fun `Should delete product by id`(){
         //arrange
        val id = "ABC4"
        every { productPersistancePort.deleteById(id) } returns Mono.empty()
        //act
        val result = productService.deleteProductById(id)
        //assert
        StepVerifier.create(result)
            .verifyComplete()
    }



}